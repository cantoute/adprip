# adprip.fr readme


Le script https://www.adprip.fr/update télécharge, anonymise et compile les
données des signataires dans le dossier https://www.adprip.fr/archive/

Toutes les 15mn le script récupère le nombre de pages et le nombre de signataires
sur la dernière page pour les N couples de lettre les plus populaires; si un de
ces nombres change, on détecte une mis a jour de la liste de signataires et
on recalcule le nombre total de signatures pour chaque couple de lettre.
On stocke le triplet (couple lettre;nombre page; nombre signataires dernière page)
dans le fichier /archive/cache.dat et archive le nouveau set de triplets definissant
le compteur dans /archive/TS.cache.dat.gz ou TS est le unix timestamp actuel.

Parallèlement, toutes les semaines, le script récupére toutes les villes de tous
les signataires, compile les données au format sqlite avec les données de l'INSEE
contenues dans /data_insee.raw.sqlite, enregistre les données analysées dans
/archive/TS.sqlite.gz ou TS est le timestamp en cours. Il sort ensuite l'ensemble
des JSONs nécessaires pour generer /map.html

## Note concernant RGPD/CNIL/etc:

L'article qui semble relevant est le suivant :

> Art. L. 558-42 du code électoral - Le fait, dans le cadre des mêmes
> opérations, de reproduire des données collectées à d'autres fins que
> celles de vérification et de contrôle ou de tenter de commettre cette
> reproduction est puni de cinq ans d'emprisonnement et de 75 000 €
> d'amende.

Le but ici est celui de vérification et de contrôle - c'est pour cela que les
données ne comprennent que les localités (i.e. pour vérifier l'absence
d'anomalie statistique, par exemple qu'une certaine localité soit empêchée de
voter).
C'est la raison principale pour laquelle le script faisant tourner le site est
open-source - pour ne pas être accusé de collecter des données à tort et à
travers.

De plus nous n'enregistrons aucune donnée nominative, aucune donnée concernant
les visiteurs du site adprip.fr (cookies ou autre - hormis les logs OVH
(apache?) qui sont hors de notre ressort), ce qui semble éliminer les problèmes
de RGPD et de CNIL. Si vous êtes un avocat/une personne bien placée pour en savoir
plus n'hésitez pas à me contacter (via gitlab).

