
//Check that feature corresponds to a departement from which we have geoJSON commune detailed data
function hasGeoJSONCommuneData(feature) {
    //         polynesie                            nvlle caledonie
    return (feature.properties.code != "987" && feature.properties.code != "988")
}

function getVillePosition(feature) {
    var lnglat = turf.centroid(feature).geometry.coordinates
    return [lnglat[1],lnglat[0]]
}

function generateShareLinks(code, nom, latlng) {
    var villeUrl = window.location.protocol + '//' + window.location.hostname + '/map.html?insee=' + code
        + '&lat=' + latlng[0] + '&lng=' + latlng[1] + '&zoom=11'
    var twitterTxt = 'Les électeurs de ' + nom + ' veulent le référendum sur la privatisation du groupe ADP.%0A%0AVoir ' + nom + ' sur @adpripfr :%0A'
    return '<div class="share-links"><a target="_blank" title="Lien direct vers '
            + nom + '" href="' + villeUrl + '"><i class="fa fa-link"></i></a>&nbsp;&nbsp;'
            + '<a target="_blank" rel="nofollow" title="Partagez les infos de '
            + nom + ' sur twitter" href="http://twitter.com/share?text='
            + twitterTxt + '&url=' + encodeURIComponent(villeUrl) + '&hashtags=RéférendumADP,Palmar&egrave;sADP"><i class="fa fa-twitter"></i></a>'
            + '<a target="_blank" rel="nofollow" title="Partagez les infos de '
            + nom + ' sur facebook" href="https://www.facebook.com/sharer/sharer.php?u='
            + encodeURIComponent(villeUrl) + '" style="margin-left: 5px"><i class="fa fa-facebook"></i></a>'
            + '</div>'
}

var MAP_POSITIONS =
[
    {0:  [-7.294922,40.195659,14.128418,52.802761]},     //metropole
    {1:  [-62.083740,15.749963,-60.853271,16.704602]},   //guadeloupe
    {2:  [-61.443787,14.272369,-60.567627,15.027033]},   //martinique
    {3:  [-55.393066,1.609285,-50.679932,6.233395]},     //guyanne
    {4:  [54.453735,-22.095820,56.755371,-20.164255]},   //reunion
    {5:  [-57.354126,46.259645,-55.052490,47.772560]},   //pierre miquelon
    {6:  [44.574280,-13.520508,45.986023,-12.297068]},   //mayotte
    {7:  [-62.924881,17.840218,-62.718544,17.983631]},   //st barth
    {8:  [-63.205032,17.981346,-62.917671,18.207502]},   //st martin
    {9:  [178.846436,-15.718239,186.113892,-11.178402]}, //wallis futuna
    {10: [-155,-28,-134,-7.5]},                          //polynesie
    {11: [160.697021,-24.547123,171.441650,-17.098792]}  //nvlle caledonie
]

function estimateMapPosition(lat, lng) {
    for(var i = 0; i < MAP_POSITIONS.length; i++) {
        var elem = MAP_POSITIONS[i][i]
        if(elem[0] <= lng && lng <= elem[2] && elem[1] <= lat && lat <= elem[3]) {
            return i
        }
    }

    console.log(lng, lat, " position non reconnue")
    return 0
}


function convertlink(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a href="' + url + '" rel="nofollow" target="_blank">' + geturl(url) + '</a>';
    })

}

function geturl(url){
    if(url.length > 20){
        return url.substr(0,20) + "...";
    } else {
        return url;
    }
}

function getDepartementFromInsee(insee) {
    if(insee.substring(0,2) == "2A" || insee.substring(0,2) == "2B") {
        return insee.substring(0,2)
    }

    var departement = insee.substring(0,2)
    if(parseInt(departement) <= 95) {
        return departement
    }

    return parseInt(insee.substring(0,3))
}


function getBlasonUrl(insee, blasons) {
    var baseUrl = blasons["base_url"]
    if(blasons["villes"][insee] != undefined) {
        return blasons["base_url"]+blasons["villes"][insee]
    }

    var departement = getDepartementFromInsee(insee)
    if(blasons["departements"][departement] != undefined) {
        return blasons["base_url"]+blasons["departements"][departement]
    }

    return ""
}

var communesGeoJson = {}
function loadCommuneGeoJson(baseUrl, code, callback) {
    if(code in communesGeoJson) {
        callback(communesGeoJson[code])
        return
    }

    $.getJSON(baseUrl + 'carto/departements/' + code + '/communes.geojson', function(data){
        communesGeoJson[code] = data;
        callback(data);
    })
}
