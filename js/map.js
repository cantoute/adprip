$(function() {
    //navigation bar
    $('#header').load('navigation.html')

    $('leaflet-control-fullscreen-button').attr('data-toggle', 'tooltip').attr('data-placement', 'right')

    $('[data-toggle="tooltip"]').tooltip()
})

var url = new URL(window.location.href),
    mapCenterDefault = [
        46.31564,
        3.562414,
    ], // center en France
    zoomDefault = 5.5,
    zoomMin = zoomDefault,
    zoomMax = 13,
    lat = url.searchParams.get('lat') || mapCenterDefault[0],
    lng = url.searchParams.get('lng') || mapCenterDefault[1],
    zoom = url.searchParams.get('zoom') || zoomDefault,
    mapCenter = [
        lat,
        lng,
    ],
    rateGradient = true,
    data_departements,
    data_villes,
    communeLayer,
    allCommuneLayer = [],
    arrDptVotes,
    depLayer,
    highestVote,
    baseUrl = '',
    // urlADPRip = baseUrl + 'archive/latest.communes.json',
    urlADPRipCommunes = baseUrl + 'archive/data_communes.json', // the one in www is broken right now
    urlADPRipDpt = baseUrl + 'archive/data_departements.json'

// if (window.location.href.includes("adprip.ml")) {
//     var proxyurl = "https://cors-anywhere.herokuapp.com/";
//     urlADPRip = proxyurl + urlADPRip;
// var proxyurl = "https://cors-anywhere.herokuapp.com/";
// urlADPRipDpt = proxyurl + urlADPRipDpt;
// }

var map = L.map('map', {
    preferCanvas: true,
    fullscreenControl: {
        pseudoFullscreen: true, // fullscreen to page width and height
        title: {
            false: 'Affichage plein écran',
            true: 'Quitter le plein écran',
        },
    },
    loadingControl: true,
}).setView(mapCenter, zoom)

// To support browser history
window.addEventListener('popstate', function(e) {
    if (e.state) {
        var latlng = e.state.latlng
        var zoom = e.state.zoom
        map.setView(latlng, zoom)
    } else {
        map.setView(mapCenter, zoomDefault)
    }
})

L.tileLayer(
    'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ2Rmc2dmZHNnIiwiYSI6ImNqeDV3c3F0YjA2NGQ0YXF1enF5ZTRkaGkifQ.tWSer1X94iZmBIz-eDquRA',
    {
        maxZoom: 18,
        attribution: `&copy; <a rel="nofollow" target="_blank" href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,
                <a rel="nofollow" target="_blank" href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>,
                Imagery &copy; <a rel="nofollow" target="_blank" href="https://www.mapbox.com/">Mapbox</a>,
                Soutiens par <a href="https://adprip.fr">ADPRip.fr</a>,
                licence (ouverte) geoJSON <a rel="nofollow" target="_blank" href="/carto/COPYING">ici</a>,
                Integration par <a rel="nofollow" target="_blank" href="https://twitter.com/emiliodib">@emiliodib</a>`,
        id: 'mapbox.light',
    }
).addTo(map)

var infoContainer
var info = L.control({ position: 'topright' })
info.onAdd = function(map) {
    infoContainer = this._div = L.DomUtil.create('div', 'info')
    $(infoContainer).css('display', 'none')
    return this._div
}
info.addTo(map)

var legendNumber = L.control({ position: 'bottomleft' })
legendNumber.onAdd = function(map) {
    var div = L.DomUtil.create('div', 'info legend'),
        grades = [
            0,
            50,
            100,
            250,
            500,
            1000,
            5000,
            10000,
        ],
        labels = []
    div.innerHTML += '<h5>Nb. de soutiens</h5>'
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' +
            getColorNumber(grades[i] + 1) +
            '"></i> ' +
            grades[i] +
            (grades[i + 1] ? ' &ndash; ' + grades[i + 1] + '<br>' : ' et +')
    }
    div.innerHTML += '<p class="help mt-2">Un facteur 10 est<br />appliqué pour les<br />départements.</p>'
    return div
}

var extentControl = L.Control.extend({
    options: {
        position: 'topleft',
    },
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'extentControl')
        container.title = "Afficher la France métropolitaine"
        $(container)
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'right')
            .addClass('leaflet-bar leaflet-control')
            .css('background', 'white url(images/icons/france.png) no-repeat 50% 50%')
            .css('width', '26px')
            .css('height', '26px')
            .css('outline', '1px black')
            .css('border-radius', '4px')
            .css('box-shadow', '0 1px 5px rgba(0,0,0,0.65)')
            .css('cursor', 'pointer')
            .hover(
                function() {
                    $(this).css('background-color', '#f4f4f4')
                },
                function() {
                    $(this).css('background-color', 'white')
                }
            )
        // TO DO non-Safari
        // var linkEl = $(container).append("<a></a>")
        $(container).on('click', moveFranceMetropole)
        return container
    },
})
map.addControl(new extentControl())

var switchGradient = L.Control.extend({
    options: {
        position: 'topleft',
    },
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'switchgradientcontrol')
        container.title = 'Gradient de couleur par nombre ou par taux de soutiens'
        $(container)
            .addClass('leaflet-bar leaflet-control')
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'right')
            .text('%')
            .css('background', 'white no-repeat 50% 50%')
            .css('width', '26px')
            .css('height', '26px')
            .css('outline', '1px black')
            .css('border-radius', '4px')
            .css('box-shadow', '0 1px 5px rgba(0,0,0,0.65)')
            .css('cursor', 'pointer')
            .css('padding-top', '1px')
            .css('padding-left', '4px')
            .css('font-weight', 'bold')
            .css('font-size', '16px')
            .hover(
                function() {
                    $(this).css('background-color', '#f4f4f4')
                },
                function() {
                    $(this).css('background-color', 'white')
                }
            )
            .on('click', function() {
                rateGradient = !rateGradient
                if (!rateGradient) {
                    $(this).text('#')
                    $(this).css('padding-left', '8px')
                    map.removeControl(legendPercent)
                    legendNumber.addTo(map)
                    for (var i = 0; i < allCommuneLayer.length; i++) {
                        allCommuneLayer[i].setStyle(function(feature) {
                            var heatMapColor = getColorNumber(feature.properties.soutiens)
                            return { fillOpacity: 0.7, weight: 0, color: '#4d004b', fillColor: heatMapColor }
                        })
                    }
                    depLayer.setStyle(function(feature) {
                        if (feature.properties.hidden != 1) {
                            var heatMapColor = getColorNumber(feature.properties.soutiens_dpt / 10) // /10
                            return { fillOpacity: 0.7, weight: 0, color: '#4d004b', fillColor: heatMapColor } // color: '#4d004b',
                        } else {
                            return { fillOpacity: 0 }
                        }
                    })
                } else {
                    $(this).text('%')
                    $(this).css('padding-left', '4px')
                    legendPercent.addTo(map)
                    map.removeControl(legendNumber)
                    for (var i = 0; i < allCommuneLayer.length; i++) {
                        allCommuneLayer[i].setStyle(function(feature) {
                            var heatMapColor = getColor(feature.properties.pourcentage * 100)
                            return { fillOpacity: 0.7, weight: 0, color: '#4d004b', fillColor: heatMapColor }
                        })
                    }
                    depLayer.setStyle(function(feature) {
                        if (feature.properties.hidden != 1) {
                            var heatMapColorDpt = getColor(feature.properties.prct_dpt * 100)
                            return { fillOpacity: 0.7, weight: 0, color: '#4d004b', fillColor: heatMapColorDpt }
                        } else {
                            return { fillOpacity: 0 }
                        }
                    })
                }
            })
        return container
    },
})
map.addControl(new switchGradient())

function getColor(d) {
    return d >= 10
        ? '#4d004b'
        : d >= 5
            ? '#810f7c'
            : d >= 3
                ? '#88419d'
                : d >= 2 ? '#8c6bb1' : d >= 1.5 ? '#8c96c6' : d >= 1 ? '#9ebcda' : d >= 0.5 ? '#bfd3e6' : '#e0ecf4'
}

var actionMarkers = new L.FeatureGroup()
map.addLayer(actionMarkers);
var switchMarkers = L.Control.extend({
    options: {
        position: 'topleft',
    },
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'switchmarkercontrol')
        container.title = 'Afficher/cacher les actions de signons.fr'
        $(container)
            .addClass('leaflet-bar leaflet-control')
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'right')
            .css('background', 'white url(img/map_icon.png) no-repeat')
            .css('background-size', 'contain')
            .css('width', '26px')
            .css('height', '26px')
            .css('outline', '1px black')
            .css('border-radius', '4px')
            .css('box-shadow', '0 1px 5px rgba(0,0,0,0.65)')
            .css('cursor', 'pointer')
            .css('padding-top', '1px')
            .css('padding-left', '4px')
            .css('font-weight', 'bold')
            .css('font-size', '16px')
            .hover(
                function() {
                    $(this).css('background-color', '#f4f4f4')
                },
                function() {
                    $(this).css('background-color', 'white')
                }
            )
            .on('click', function() {
                if(map.hasLayer(actionMarkers)) {
                    map.removeLayer(actionMarkers);
                } else {
                    map.addLayer(actionMarkers);
                }
            })
        return container
    },
})
map.addControl(new switchMarkers())

var mairiesMarkers = new L.FeatureGroup()
//map.addLayer(mairiesMarkers);
var switchMairiesMarkers = L.Control.extend({
    options: {
        position: 'topleft',
    },
    onAdd: function(map) {
        var container = L.DomUtil.create('div', 'switchmairiesmarkercontrol')
        container.title = 'Afficher/cacher les mairies recevant des CERFAs'
        $(container)
            .addClass('leaflet-bar leaflet-control')
            .attr('data-toggle', 'tooltip')
            .attr('data-placement', 'right')
            .text('C')
            .css('background', 'white url(img/map_mairies.png) no-repeat')
            .css('background-size', 'contain')
            .css('width', '26px')
            .css('height', '26px')
            .css('outline', '1px black')
            .css('border-radius', '4px')
            .css('box-shadow', '0 1px 5px rgba(0,0,0,0.65)')
            .css('cursor', 'pointer')
            .css('padding-top', '1px')
            .css('text-align', 'center')
            .css('font-weight', 'bold')
            .css('font-size', '16px')
            .hover(
                function() {
                    $(this).css('background-color', '#f4f4f4')
                },
                function() {
                    $(this).css('background-color', 'white')
                }
            )
            .on('click', function() {
                if(map.hasLayer(mairiesMarkers)) {
                    map.removeLayer(mairiesMarkers);
                } else {
                    map.addLayer(mairiesMarkers);
                }
            })
        return container
    },
})
map.addControl(new switchMairiesMarkers())

var legendPercent = L.control({ position: 'bottomleft' })
legendPercent.onAdd = function(map) {
    var div = L.DomUtil.create('div', 'info legend'),
        grades = [
            0,
            0.5,
            1,
            1.5,
            2,
            3,
            5,
            10,
        ],
        labels = []
    div.innerHTML += '<h5>Taux de soutiens</h5>'
    // loop through density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' +
            getColor(grades[i] + 1) +
            '"></i> ' +
            grades[i] +
            (grades[i + 1] ? ' &ndash; ' + grades[i + 1] + '%<br>' : '% et +')
    }
    return div
}
legendPercent.addTo(map)

var geocoder = L.Control.Geocoder.nominatim({
    // Limit set to 10 because we have 10 'Saint-Paul' in France
    geocodingQueryParams: { countrycodes: 'fr', featuretype: 'city', limit: 10 },
})

if (URLSearchParams && location.search) {
    // parse /?geocoder=nominatim from URL
    var params = new URLSearchParams(location.search)
    var geocoderString = params.get('geocoder')
    if (geocoderString && L.Control.Geocoder[geocoderString]) {
        console.log('Using geocoder', geocoderString)
        geocoder = L.Control.Geocoder[geocoderString]()
    } else if (geocoderString) {
        console.warn('Unsupported geocoder', geocoderString)
    }
}

var control = L.Control
    .geocoder({
        geocoder: geocoder,
        placeholder: 'Chercher une commune...',
        expand: 'click',
        defaultMarkGeocode: false,
        position: 'bottomright',
    })
    .on('markgeocode', function(e) {
        map.setView(e.geocode.center, 11)
        var latlngPoint = new L.LatLng(e.geocode.center.lat, e.geocode.center.lng)
        var polygonCible = leafletPip.pointInLayer(latlngPoint, depLayer, true)
        if (allCommuneLayer.length != 0) {
            var foundCommunes = false
            for (var i = 0; i < allCommuneLayer.length; i++) {
                var polygonCommune = leafletPip.pointInLayer(latlngPoint, allCommuneLayer[i], false)
                if (polygonCommune.length != 0) {
                    polygonCommune[0].fireEvent('click', {
                        latlng: [
                            e.geocode.center.lat,
                            e.geocode.center.lng,
                        ],
                    })
                    foundCommunes = true
                    i = allCommuneLayer.length
                }
            }
        }
        if (!foundCommunes) {
            polygonCible[0].fireEvent('click', {
                latlng: [
                    e.geocode.center.lat,
                    e.geocode.center.lng,
                ],
            })
            var commmuneClicked = false
            // TO DO
            // Callback
            setTimeout(function() {
                for (var i = 0; i < allCommuneLayer.length; i++) {
                    var polygonCommune = leafletPip.pointInLayer(latlngPoint, allCommuneLayer[i], false)
                    if (polygonCommune.length != 0) {
                        polygonCommune[0].fireEvent('click', {
                            latlng: [
                                e.geocode.center.lat,
                                e.geocode.center.lng,
                            ],
                        })
                        i = allCommuneLayer.length
                    }
                }
            }, 800)
        }
    })
    .addTo(map)

function getColorNumber(d) {
    return d >= 10000
        ? '#7f0000'
        : d >= 5000
            ? '#b30000'
            : d >= 1000
                ? '#d7301f'
                : d >= 500 ? '#ef6548' : d >= 250 ? '#fc8d59' : d >= 100 ? '#fdbb84' : d >= 50 ? '#fdd49e' : '#fee8c8'
}

var departements;
var outreMer;
var outreMerCom;
var blasons;
var fr_data;
var fr_required_raw;

$.when(
    $.getJSON(urlADPRipDpt, function(data) {
        data_departements = data
        fr_data = data_departements["00"].soutiens;
        fr_required_raw = data_departements["00"].electeurs;
    }),
    $.getJSON(urlADPRipCommunes, function(data, textStatus, jqXHR) {
        data_villes = data
        $('#majdate').text(
            'Données mises à jour : ' +
                new Date(Date.parse(jqXHR.getResponseHeader('last-modified'))).toLocaleDateString('fr-FR')
        )
    }),
    $.getJSON('carto/departements.json', function(data) {
        departements = data;
    }),
    $.getJSON('carto/departements-97x.json', function(data) {
        outreMer = data;
    }),
    $.getJSON('carto/departements-98x.json', function(data) {
        outreMerCom = data;
    }),
    $.getJSON('carto/blasons.json', function(data) {
        blasons = data;
    }),
    $.getJSON('carto/mairies.json', function(data) {
        for (var mIdx = 0; mIdx < data.length; mIdx++) {
            var circleMarker = L.circle([data[mIdx][1], data[mIdx][2]], 800,
                { color: '#3388ff' })
            circleMarker.bindPopup(
                '<h6>Vous pouvez d&eacute;poser un soutien papier (formulaire CERFA)'
                + ' &agrave; cette adresse :</h6>'
                + '<strong>' + data[mIdx][0].replace(' - ', '<br/>') + '</strong><br/><br/>'
                + '<div class="text-right"><a rel="nofollow" target="_blank" '
                + 'href="https://www.referendum.interieur.gouv.fr/formulaire-papier">'
                + 'En savoir plus...</a></div>')
            mairiesMarkers.addLayer(circleMarker)
        }
    }),
    $.getJSON('https://referendum.signons.fr/api/active-actions/', function(data) {
        var myIcon = L.icon({iconUrl: '/img/map_icon.png', iconSize: [30, 30]});
        for(var sIdx = 0; sIdx < data["ldp:contains"].length; sIdx++) {
            var elem = data["ldp:contains"][sIdx];
            var id = elem["@id"].split("/").slice(-2, -1)[0];
            var popupText = '<div><h6>' + elem["name"] + '</h6>'
                + '<small>Action <a href="https://signons.fr/" rel="nofollow" target="_blank"><i>signons.fr</i></a> à ' + elem["city"] + ' le <b>' + elem["date"] + ' (' + elem["start_time"] + '~' + elem["end_time"] +')</b>.</small>'
                + '<p>' + convertlink(elem["description"]) + '</p>'
                + '<center><a href="https://referendum.signons.fr/les-actions/action-detail/@https~@~_~_referendum~!signons~!fr~_api~_actions~_'+id+'~_" rel="nofollow" target="_blank"><p class="participe">Je participe!</p></a></center>'
                + '</div>';
            actionMarkers.addLayer(L.marker([data["ldp:contains"][sIdx]["lat"],data["ldp:contains"][sIdx]["lng"]], {icon: myIcon}).bindPopup(popupText));
        }
    })
).then(function() {
    // Ajoute les outre-mers aux departements de la metropole
    Array.prototype.push.apply(departements.features, outreMer.features)
    Array.prototype.push.apply(departements.features, outreMerCom.features)

    // Ajoute les français de l'étranger
    var fr_counter = new Intl.NumberFormat('fr-FR').format(fr_data),
        fr_required = new Intl.NumberFormat('fr-FR').format(fr_required_raw),
        fr_percent = fr_data / fr_required_raw * 100,
        fr_percentFormatted = new Intl.NumberFormat("fr-FR", { maximumFractionDigits: 2 }).format(fr_percent);

    $('#location').append('<option value="">Français de l\'Etranger: '+fr_counter+' soutiens / '+fr_required + ' (' + fr_percentFormatted + '%)</option>');

    // Ajoute les départements
    depLayer = L.geoJSON(departements, {
        style: function(feature) {
            feature.properties.soutiens = data_departements[feature.properties.code].soutiens
            feature.properties.electeurs = data_departements[feature.properties.code].electeurs;
            feature.properties.pourcentage = feature.properties.soutiens / feature.properties.electeurs

            if (!rateGradient) {
                if (feature.properties.hidden != 1) {
                    var heatMapColor = getColorNumber(feature.properties.soutiens_dpt / 10) // /10
                    return { fillOpacity: 0.7, weight: 0, fillColor: heatMapColor } // color: '#4d004b',
                } else {
                    return { fillOpacity: 0 }
                }
            } else {
                if (feature.properties.hidden != 1) {
                    var heatMapColorDpt = getColor(feature.properties.prct_dpt * 100)
                    return { fillOpacity: 0.7, weight: 0, color: '#4d004b', fillColor: heatMapColorDpt }
                } else {
                    return { fillOpacity: 0 }
                }
            }
        },
        onEachFeature: onEachFeature,
    }).addTo(map)

    // hide loading
    document.getElementById('parsing').style.display = 'none'

    // exemple de la Somme : ?lat=49.9685061&lng=1.7309293&zoom=9
    // Pyrénées Orientales : ?lat=42.65&lng=2.45&zoom=10
    // Aude : ?lat=43.5&lng=2.42&zoom=10
    // Ariège : ?lat=42.983&lng=1.4667&zoom=10
    if (url.searchParams.get('lat') && url.searchParams.get('lng') && url.searchParams.get('zoom')) {
        var latlngPointParams = new L.LatLng(lat, lng)
        var departementCible = leafletPip.pointInLayer(latlngPointParams, depLayer, true)
        departementCible[0].fireEvent('click', {
            latlng: [
                lat,
                lng,
            ],
        })
    }
})
// end of getJSON('carto/departements.json'...


var lastPopupDptCode = "00" //pour se souvenir de quel departement on a hover en dernier
function onEachFeature(feature, layer) {
    if (feature.properties.code.length > 3) {
        map.fireEvent('dataloading', event)
    }

    layer.on('click', function(e) {
        var maxPrct = 0.01

        if (feature.properties.code.length <= 3) {
            var newZoom = Math.max(map.getZoom(), 7.5)
            // Add gps coordinates to window url
            if (e.latlng) {
                updateBrowserLocation(e.latlng, newZoom)
            }

            if(!hasGeoJSONCommuneData(feature)) { //do not try to load geoJSON if we don't have it
                return
            }

            layer.closePopup()
            map.fireEvent('dataloading', event)
            map.setView(e.latlng, newZoom)
            map.fireEvent('dataloading', event)

            loadCommuneGeoJson(baseUrl, feature.properties.code, function(communeJson) {
                communeJson.features.forEach(function(feature) {
                    var communeDataToAdd = data_villes[feature.properties.code]
                    if (communeDataToAdd) {
                        feature.properties.electeurs = communeDataToAdd.electeurs
                        feature.properties.soutiens = communeDataToAdd.soutiens
                        feature.properties.pourcentage = feature.properties.soutiens / feature.properties.electeurs
                        feature.properties.homonymes = communeDataToAdd.homonymes
                        if (
                            feature.properties.electeurs == 0 ||
                            feature.properties.electeurs == undefined ||
                            feature.properties.pourcentage == undefined
                        ) {
                            feature.properties.pourcentage = 0
                        }

                        if (feature.properties.pourcentage > maxPrct) {
                            maxPrct = feature.properties.pourcentage
                        }
                    }

                    setTimeout(function() {
                        layer.setStyle({
                            fillOpacity: 0,
                        })
                        feature.properties.hidden = 1
                        map.fireEvent('dataload', event)
                    }, 500)
                })

                communeLayer = L.geoJSON(communeJson, {
                    style: function(feature) {
                        if (!rateGradient) {
                            var heatMapColor = getColorNumber(feature.properties.soutiens)
                            return { fillOpacity: 0.7, weight: 0, color: '#4d004b', fillColor: heatMapColor }
                        } else {
                            var heatMapColor = getColor(feature.properties.pourcentage * 100)
                            return { fillOpacity: 0.7, weight: 0, color: '#4d004b', fillColor: heatMapColor }
                        }
                    },
                    onEachFeature: onEachFeature,
                }).addTo(map)
                allCommuneLayer.push(communeLayer)

                // Re-dessine les markers pour les mettre au dessus
                if (map.hasLayer(mairiesMarkers)) {
                    map.removeLayer(mairiesMarkers)
                    map.addLayer(mairiesMarkers)
                }
            })
        }
    })

    var popupContent = '<div><div style="float: left; margin-right:10px;"><center><img style="max-height:90px;" src="'+getBlasonUrl(feature.properties.code, blasons)+'"></center></div><div style="margin-top:10px;">'

    if (feature.properties.electeurs > 0) {
        popupContent +=
            '<h6 style="white-space: nowrap;">' +
            feature.properties.nom +
            '</h6>Électeurs : ' +
            feature.properties.electeurs +
            ', <br />Soutiens acceptés : ' +
            feature.properties.soutiens +
            ', <br />Pourcentage : ' +
            Math.round(feature.properties.pourcentage * 10000) / 100 +
            ' %'
    } else if (feature.properties.electeurs == 0) {
        popupContent +=
            '<h6>' +
            feature.properties.nom +
            '</h6>' +
            'Soutiens acceptés : ' +
            feature.properties.soutiens +
            "<br />Électeurs : nombre d'electeurs inconnu ou aucun électeur"
    } else {
        popupContent +=
            '<h6>' +
            feature.properties.nom +
            '</h6>' +
            "Nombre d'électeurs inconnu.<br />Autres communes avec même nom.<br />" +
            "Impossible d'estimer le nombre de soutiens."
    }

    if (typeof feature.properties.homonymes !== 'undefined') {
        if (feature.properties.homonymes.length > 0) {
            if (feature.properties.electeurs >= 0) {
                popupContent +=
                    "<p class='text-muted small'>Le nombre de soutiens pour cette commune est estimé proportionnellement à ses communes homonymes (codes INSEE) : "
            } else {
                popupContent += '<p>Codes INSEE de ses communes homonymes : '
            }
            popupContent += JSON.stringify(feature.properties.homonymes)
                .replace(/[\[\]&\"]+/g, '')
                .replace(/[\,]+/g, ', ')
            popupContent += '</p>'
        } else {
            popupContent += '<br />'
        }
    }

    var popupContentDpt =
        '<p><strong>' +
        feature.properties.nom +
        '</strong> Electeurs : ' +
        feature.properties.electeurs +
        ', Soutiens acceptés : ' +
        feature.properties.soutiens +
        ', Pourcentage : ' +
        Math.round(feature.properties.pourcentage * 10000) / 100 +
        ' %</p>'

    //COMs dont on a pas les donnees geoJSON par commune : ajout recherche commune par texte
    if(!hasGeoJSONCommuneData(feature)) {
        var nom = feature.properties.nom,
            latlng = getVillePosition(feature);
        popupContentDpt += "<br /><iframe frameBorder=\"0\" style=\"width:100%;height:100%;\" src=\"/fuzzy_search.html?nom="+nom+"&code="+feature.properties.code+"&lat="+latlng[0]+"&lng="+latlng[1]+"\"></iframe>"
    }

    if (feature.properties.code.length > 3) {
        layer.bindPopup(popupContent + generateShareLinks(feature.properties.code, feature.properties.nom, getVillePosition(feature)) + "</div></div>", {minWidth: 250});
        layer.on('click', function(e) {
            // Add gps coordinates to window url
            if (e.latlng) {
                updateBrowserLocation(e.latlng, map.getZoom())
            }
        })
        layer.on('mouseover', function(e) {
            layer.setStyle({
                weight: 0.5,
            })
            var content = this._popup.getContent().replace(/<img[^>]*>/g,"");
            $(infoContainer).css('display', 'block')
            $(infoContainer).html(content)
        })
        layer.on('mouseout', function(e) {
            layer.setStyle({
                weight: 0.0,
            })
            $(infoContainer).css('display', 'none')
        })
    } else {
        layer.on('mouseover', function(e) {
            layer.setStyle({
                weight: 0.5,
            })
            if(feature.properties.code != lastPopupDptCode) {
                layer.bindPopup(popupContentDpt)
                var content = this._popup.getContent()
                $(infoContainer).css('display', 'block')
                $(infoContainer).html(content)
                layer.unbindPopup()
                lastPopupDptCode = feature.properties.code
            }
        })
        layer.on('mouseout', function(e) {
            layer.setStyle({
                weight: 0.0,
            })

        if(hasGeoJSONCommuneData(feature)) { //hide commune search only if iframe doesn't exist
            $(infoContainer).css('display', 'none')
            lastPopupDptCode = "00"
        }
        })
    }
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? 0 : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function updateBrowserLocation(latlng, zoom, insee = 0) {
    zoom = Math.max(Math.min(zoom, zoomMax), zoomMin)

    var newLocation = window.location.pathname + '?lat=' + parseFloat(latlng.lat || latlng[0]).toFixed(4) + '&lng=' + parseFloat(latlng.lng || latlng[1]).toFixed(4)
                                       + '&zoom=' + zoom;

    if(insee != 0) {
        newLocation += '&insee=' + insee;
    } else if(getUrlParameter("insee") != 0) {
        newLocation += '&insee=' + getUrlParameter("insee")
    }

    $("select#location").prop('selectedIndex', estimateMapPosition(latlng.lat || latlng[0], latlng.lng || latlng[1]))
    window.history.pushState(
        { latlng: latlng, zoom: zoom },
        null,
        newLocation
    )
}

function moveFranceMetropole() {
    map.setView(mapCenterDefault, zoomDefault)
    $("select#location").prop('selectedIndex', 0);

    // clean up url when back to default map
    window.history.pushState(
        { latlng: mapCenterDefault, zoom: zoomDefault },
        null,
        window.location.pathname
    )
}
