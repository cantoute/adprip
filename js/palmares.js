$(document).ready(function() {
    var url = new URL(window.location.href)

    // Pour les tests en local, il suffit de charger la page /palmares.html?remote=1
    var baseUrl = url.searchParams.has('remote') ? 'https://www.adprip.fr/' : ''

    var villes = null
    var departements = null

    $('#header').load(baseUrl + 'navigation.html')

    function formatDepartements(nom) {
        return nom
            .replace(/d([A-Z])/g, "d'$1")
            .replace(/([a-z0-9])([A-Z])/g, '$1 $2')
            .replace('St ', 'Saint ')
            .replace('Miquelon', 'et Miquelon')
            .replace('Territoirede', 'Territoire de')
            .replace(' Et ', ' et ')
            .replace(' Caledonie', '-Calédonie')
            .replace('Fr Etranger', "Français de l'étranger")
    }
    function formatArrondissements(nom) {
        return nom.replace(/([a-zA-Z]+)([0-9]+)(er?)Arrondissement/, '$1&nbsp;$2$3&nbsp;Arrondissement')
            .replace(/-/g, '&#8209;')
    }

    // Palmares des departements
    $.when(
        $.getJSON(baseUrl + 'archive/data_departements.json', function(data) {
            departements = data
        })
    ).then(function() {
        var dptFilter = $('#departementFilter')

        $.each(departements, function(deptCode, deptObj) {
            deptObj.nom = formatDepartements(deptObj.nom)
            dptFilter.append(
                $('<option>', { value: deptCode, text: deptCode + ' - ' + deptObj.nom})
            )

            deptObj.dept_nom = deptObj.dept_nom.replace(/-/g, '&#8209;') //utiliser tirets insecables pour le tableau
        })

        // Select departement from url
        var urlDepartement = url.searchParams.get('dpt')
        if ($('#departementFilter option[value="' + urlDepartement + '"]').length > 0) {
            $('#departementFilter').val(urlDepartement)
        }

        var dataForTable = $.map(departements, function(deptObj, deptCode) {
            var pourcent = 0
            if (deptObj.soutiens > 0 && deptObj.soutiens < deptObj.electeurs) {
                pourcent = 100.0 * deptObj.soutiens / deptObj.electeurs
            }
            return [[
                deptCode,
                deptObj.nom,
                pourcent,
                deptObj.soutiens,
                deptObj.electeurs
            ]]
        })
        .sort(function(d1, d2) {
            return d2[2] - d1[2]
        })
        .map(function(item, index) {
            item.unshift(index + 1)
            item[3] = item[3].toFixed(2)
            return item
        })

        $('#PalmaresDepartements').DataTable({
            data: dataForTable,
            deferRender: true,
            search: {
                search: url.searchParams.get('q') || ''
            },
            displayStart: parseInt(url.searchParams.get('p'), 10) || 0,
            pageLength: parseInt(url.searchParams.get('pl'), 10) || 10,
            columns: [
                {
                    title: '',
                    className: 'text-right small font-weight-bold',
                    render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '', '&nbsp;e')
                },
                {
                    title: 'Code',
                    className: 'text-right'
                },
                {
                    title: 'D&eacute;partement'
                },
                {
                    title: 'Pourcentage',
                    render: $.fn.dataTable.render.number('&nbsp;', ',', 2, '', '&nbsp;%'),
                    className: 'text-right',
                },
                {
                    title: 'Soutiens',
                    render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                    className: 'text-right'
                },
                {
                    title: "Nombre d'&eacute;lecteurs",
                    render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                    className: 'text-right',
                },
            ],
            order: [
                [
                    3,
                    'desc',
                ],
            ],
        })
    })

    //~ // Palmares des villes
    var palmaresCommunesTable
    $.when(
        $.getJSON(baseUrl + 'archive/data_communes.json', function(data) {
            villes = data
        })
    ).then(function() {
        var dataForTable = $.map(villes, (function(ville, codeInsee) {
            var pourcent = 0
            if (ville.electeurs > 0 && ville.soutiens < ville.electeurs) {
                pourcent = 100.0 * ville.soutiens / ville.electeurs
            }
            var departement = codeInsee.substr(0, 2)
            if (departement === '97' || departement === '98') {
                departement = codeInsee.substr(0, 3)
            }
            return [[
                departement,
                codeInsee,
                formatArrondissements(ville.nom),
                pourcent,
                ville.soutiens,
                ville.electeurs,
            ]]
        }))
        .sort(function(c1, c2) {
            return c2[3] - c1[3]
        })
        .map(function(item, index) {
            item.unshift(index + 1)
            item[4] = item[4].toFixed(2)
            return item
        })

        palmaresCommunesTable = $('#PalmaresCommunes').DataTable({
            data: dataForTable,
            deferRender: true,
            search: {
                search: url.searchParams.get('q') || ''
            },
            displayStart: parseInt(url.searchParams.get('p'), 10) || 0,
            pageLength: parseInt(url.searchParams.get('pl'), 10) || 10,
            columns: [
                {
                    title: '',
                    className: 'text-right small font-weight-bold',
                    render: $.fn.dataTable.render.number('&nbsp;', ',', 0, '', '&nbsp;e')
                },
                {
                    title: 'D&eacute;pt.',
                    className: 'text-right'
                },
                {
                    title: 'Insee',
                    className: 'text-right',
                    visible: false
                },
                {
                    title: 'Commune'
                },
                {
                    title: 'Pourcentage',
                    render: $.fn.dataTable.render.number('&nbsp;', ',', 2, '', '&nbsp;%'),
                    className: 'text-right',
                },
                {
                    title: 'Soutiens', render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                    className: 'text-right'
                },
                {
                    title: "Nombre d'&eacute;lecteurs",
                    render: $.fn.dataTable.render.number('&nbsp;', ',', 0),
                    className: 'text-right',
                },
            ],
            order: [
                [
                    4,
                    'desc',
                ],
            ],
            initComplete: function(settings, json) {
                $('#loadingBox').hide()
                if (url.hash === '#communes' || 'com' === url.searchParams.get('vue')) {
                    $('#pills-communes-tab').trigger('click')
                }
            },
        })
    })

    //~ // Search sans accents
    $.fn.dataTableExt.ofnSearch['string'] = function(data) {
        return !data
            ? ''
            : typeof data === 'string'
              ? data
                    .replace(/\n/g, ' ')
                    .replace(/[áâàä]/g, 'a')
                    .replace(/[éêèë]/g, 'e')
                    .replace(/[íîï]/g, 'i')
                    .replace(/[óôö]/g, 'o')
                    .replace(/[úüù]/g, 'u')
                    .replace(/[ÿ]/g, 'y')
                    .replace(/ñ/g, 'n')
                    .replace(/æ/g, 'ae')
                    .replace(/œ/g, 'oe')
                    .replace(/ç/g, 'c')
              : data
    }

    // Filtres
    $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
        // Pas de filtre pour la vue par departement
        if (settings.sTableId === 'PalmaresDepartements') {
            return true
        }
        var dptFilter = $('#departementFilter').val()
        var arrFilter = $('#arrondissementFilter').val()
        var min = parseInt($('#minElecteurs').val(), 10)
        var max = parseInt($('#maxElecteurs').val(), 10)
        var dataDpt = data[1]
        var dataVille = data[3]
        var dataElecteurs = parseInt(data[6]) || 0

        if (
            (isNaN(min) || dataElecteurs >= min) &&
            (isNaN(max) || dataElecteurs <= max) &&
            (!dptFilter || dataDpt === dptFilter) &&
            (arrFilter === '1' || !dataVille.includes('Arrondissement'))
        ) {
            return true
        }
        return false
    })

    // Populate filters
    $('#minElecteurs').val(parseInt(url.searchParams.get('min'), 10) || '')
    $('#maxElecteurs').val(parseInt(url.searchParams.get('max'), 10) || '')
    $('#arrondissementFilter').val(url.searchParams.get('arr') === '0' ? 0 : 1)

    // Event listener to the two range filtering inputs to redraw on input
    $('#minElecteurs, #maxElecteurs').keyup(
        _.debounce(function() {
            palmaresCommunesTable.draw()
        }, 300)
    )
    $('#departementFilter, #arrondissementFilter').change(
        _.debounce(
            function() {
                palmaresCommunesTable.draw()
            },
            300,
            { leading: true }
        )
    )
    $('button.toggle-vis').on('click', function (e) {
        e.preventDefault()
        // Get the column API object
        var column = palmaresCommunesTable.column($(this).attr('data-column'))
        // Change button text
        if (column.visible()) {
            $(this).text($(this).text().replace('Masquer', 'Afficher'))
        } else {
            $(this).text($(this).text().replace('Afficher', 'Masquer'))
        }
        // Toggle the visibility
        column.visible(!column.visible())
    })
})
